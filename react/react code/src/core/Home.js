import React from "react";
import Posts from "../post/Posts";
import { Link } from 'react-router-dom';

const Home = () => (
  <div>
    <div className="jumbotron" >
      <h2>Welcome to Twitter for Instacar</h2>
      
                <Link to={`/post/create`}   className="btn btn-raised btn-dark btn-sm">
                   Tweet
                </Link>
    </div>
    <div className="container">
      <Posts />
    </div>
  </div>
);

export default Home;
